# Bachelor Thesis Project: MSP-430F5529LP Setup

## Repository Overview
This repository contains the code developed for a bachelor's thesis. It is designed to configure the MSP-430F5529 microcontroller for specific operations such as clock settings, ADC configurations, and data handling.

## Configuration Details

### Clock and Core Setup
- **SMCLK**: Proper System Master Clock setup.
- **Vcore**: Voltage core settings optimized.

### ADC and Timer Configuration
- **ADC_12**: Configured to collect data at 1-second intervals.
- **Timer_A**: Set to trigger actions every 1000 ms and 5000 ms.

### Data Handling
- Data from ADC_12 is stored in a buffer, which is written to an SD card every 5 seconds. The buffer is then cleared to free up space.

## Documentation
- The comprehensive documentation for this project is available in a PDF format, which is the complete bachelor's thesis.
- The code is commented in Slovak. For an English version of the code, refer to earlier versions of this repository.

## SD Card Interfacing
- The functionality to write to the SD card has been adapted from [Faysal08's MSP430F5529 SD Card Interfacing](https://github.com/Faysal08/MSP430F5529_SD_CARD_INTERFACING).

## Pin Configuration

### ADC Connection
- **ADC Input**: P1.0
- **Ground**: GND

### SD Card Connection
- **Power**: 3.3V
- **Chip Select (CS)**: P3.5
- **Master Out Slave In (MOSI)**: P3.0
- **Serial Clock (SCK)**: P3.2
- **Master In Slave Out (MISO)**: P3.1
- **Ground**: GND
