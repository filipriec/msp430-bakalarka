import serial
import time

# Setup serial connection (ensure you use the correct port name)
ser = serial.Serial('/dev/ttyACM1', 9600)

def format_time(seconds):
    # Converts seconds to "MM:SS" format
    m, s = divmod(seconds, 60)
    return "{:02}:{:02}".format(m, s)

try:
    while True:
        line = ser.readline().decode().strip()  # Read and decode the line from UART
        if line:
            parts = line.split()  # Split the line into parts
            if len(parts) > 1 and parts[1].isdigit():
                # If there is a second part and it's a digit, format it as time
                time_seconds = int(parts[1]) * 5  # Convert the interval count to seconds
                formatted_time = format_time(time_seconds)
                print(f"{parts[0]} {formatted_time}")  # Print ADC value and formatted time
            else:
                print(line)  # Print the line normally if there's no time data
except KeyboardInterrupt:
    ser.close()  # Close the serial port gracefully
