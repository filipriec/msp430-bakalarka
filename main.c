
#include <msp430.h>
#include <stdbool.h>
#include <stdio.h>
#include "ff.h"
#include "diskio.h"

#include <string.h>  // Pre funkciu strlen
#include <stdlib.h>

// Definícia makier pre nastavenie GPIO pre LED1 a LED2
#define LED1_INIT() {P1DIR |= BIT0; P1OUT &= ~BIT0;}  // Inicializácia pinu LED1 na P1.0
#define LED2_INIT() {P4DIR |= BIT7; P4OUT &= ~BIT7;}  // Inicializácia pinu LED2 na P4.7

// Definícia makier na prepnutie LED1 a LED2
#define LED1_TOGGLE() {P1OUT ^= BIT0;}  // Prepnutie pinu LED1
#define LED2_TOGGLE() {P4OUT ^= BIT7;}  // Prepnutie pinu LED2

#define TIMER_INTERVAL_1MS 25000  // Časovač nastavený na interval 1ms pri 25MHz
#define BUFFER_SIZE 5120          // Veľkosť buffera pre operácie ukladania.


void initCLK(void);                // Funkcia na inicializáciu systémového clock signálu.
void SetVcoreUp (unsigned int level);  // Funkcia na zvýšenie napätia jadra.
FRESULT WriteFile(char*, char*, WORD); // Funkcia na zápis dát do súboru.
void fat_init(void);                   // Funkcia na inicializáciu súborového systému FAT.

FIL file;                                               /* Otvorený súbor */
FATFS fatfs;                                            /* Súborový systém */
DIRS dir;                                               /* Objekt priečinka */
FRESULT errCode;                                        /* Kód chyby */
FRESULT res;                                            /* Výsledok */
UINT bytesRead;                                         /* Počet prečítaných bajtov */
UINT read;                                              /* Objekt prečítaných bajtov */


volatile unsigned int adcResult = 0;  // Premenná na uchovanie výsledkov z ADC konverzií.
unsigned char MST_Data, SLV_Data;     // Premenné pre dáta master slave, pre protokoly typu SPI a podobne.
BYTE buffer[32];                      // Všeobecný buffer s pevnou veľkosťou.
int result=1;                         // Premenná na uchovanie výsledkov operácií, pôvodne nastavená na true/1.

// Timer nastavenie funkcie
void setupTimer(void) {
  // Konfigurácia kontrolného registra časovača A0
  TA0CTL = TASSEL__SMCLK | MC__UP | TACLR;          // Výber SMCLK ako zdroja, režim s počítaním nahor, vynulovanie časovača
  TA0CCR0 = TIMER_INTERVAL_1MS;                     // Nastavenie registra zachytenia/porovnania pre interval 1ms
  TA0CCTL0 = CCIE;                                  // Povolenie prerušenia pre porovnanie CCR0
}

// Inicializácia ADC
void initGPIO(void) {
  // Inicializácia vstupného pinu ADC A0
  P6SEL |= BIT0;  // P6.0 ako vstup ADC
}

// Nastavenie ADC
void setupADC(void) {
  ADC12CTL0 = ADC12SHT02 | ADC12ON;        // Čas vzorkovania 16 ADC12CLK cyklov, ADC12 zapnuté
  ADC12CTL1 = ADC12SHP;                    // Použiť časovač vzorkovania
  ADC12MCTL0 = ADC12SREF_0 | ADC12INCH_0;  // Vr+=AVcc a Vr-=AVss, vstupný kanál A0
  ADC12IE = 0x01;                          // Povolenie prerušenia ADC pre MEM0
  ADC12CTL0 |= ADC12ENC;                   // Povolenie konverzie
}

// Funkcia na inicializáciu UART
void setupUART(void) {
  P4SEL |= BIT4 + BIT5;  // Pridelenie pinov P4.4 a P4.5 k funkciám UART

  UCA1CTL1 |= UCSWRST;                      // Udržanie modulu UART v stave resetu
  UCA1CTL1 |= UCSSEL_2;                     // Výber SMCLK ako hodinového zdroja pre UART
  UCA1BR0 = 162;                            // Nastavenie baudovej rýchlosti 9600 pri frekvencii 25MHz
  UCA1BR1 = 0;                              // Vyšší bajt baudovej rýchlosti nie je potrebný
  UCA1MCTL = UCBRS_0 + UCBRF_0 + UCOS16;    // Výber druhého modulačného stupňa (UCBRSx = 0), prvý modulačný stupeň (UCBRFx=13), a povolenie prevzorkovania (UCOS16)
  UCA1CTL1 &= ~UCSWRST;                     // Uvoľnenie modulu UART pre operáciu
  UCA1IE |= UCRXIE;                         // Povolenie prerušenia RX
}

// Funkcia na odosielanie znaku prostredníctvom UART
void uartSendChar(char c) {
  while (!(UCA1IFG & UCTXIFG));             // Čakanie, kým bude TX buffer pripravený na prijatie nových dát
  UCA1TXBUF = c;                            // Odoslanie znaku
}

// Funkcia na odosielanie reťazcov prostredníctvom UART
void uartSendString(char* str) {
  while (*str) {
    uartSendChar(*str++);                   // Odosielanie znakov jedného po druhom
  }
}

// Funkcia na odosielanie číselných hodnôt vo forme stringu prostredníctvom UART
void uartSendNumber(unsigned int num) {
  if (num == 0) {
    uartSendChar('0');
  } else {
    char str[10];
    int i = 9;
    str[i] = '\0';                          // Ukončenie reťazca nulovým znakom
    i--;
    while (num > 0 && i >= 0) {
      str[i] = (num % 10) + '0';            // Konverzia poslednej cifry čísla na znak
      num /= 10;                            // Odstránenie poslednej cifry z čísla
      i--;
    }
    uartSendString(&str[i + 1]);            // Odoslanie vygenerovaného stringu čísla
  }
  uartSendChar('\n');                       // Odoslanie znaku nového riadku po čísle
}


// Pomocná funkcia na konverziu hodnoty typu unsigned int na string
void uint_to_str(unsigned int num, char *str) {
  char *start = str;
  do {
    *str++ = '0' + num % 10;
    num /= 10;
  } while (num > 0);

  *str-- = '\0';                            // Ukončenie reťazca nulovým znakom a posunutie na posledný znak

  // Reverzia stringu
  char temp;
  while (start < str) {
    temp = *start;
    *start = *str;
    *str = temp;
    start++;
    str--;
  }
}

void main(void){

  WDTCTL = WDTPW+WDTHOLD;  // Zastavenie watchdog timera

  initCLK();               // Inicializácia systémových hodín pre optimálnu prevádzkovú rýchlosť.
  LED1_INIT();             // Inicializácia LED1 pre signalizačné a debugovacie výstupy.
  LED2_INIT();             // Inicializácia LED2 pre signalizačné a debugovacie výstupy.
  setupTimer();            // Konfigurácia hardvérového časovača pre periodické prerušenia.
  initGPIO();              // Nastavenie vstupov/výstupov všeobecného účelu.
  setupADC();              // Inicializácia ADC pre čítanie analógových signálov.
  setupUART();             // Príprava UART pre sériovú komunikáciu.
  fat_init();              // Príprava súborového systému pre operácie so súbormi.

  __enable_interrupt();    // Globálne povolenie prerušení pre MCU.
  __bis_SR_register(GIE);  // Nastavenie globálneho bitu povolenia prerušení v stavovom registri.

  while (1) {

  }
}

// Globálne premenné pre sledovanie času v rutinách obsluhy prerušení.
volatile unsigned int fiveSecCounter = 0;  // Počítadlo počtu 5-sekundových intervalov.
volatile unsigned int intervalCounter = 0; // Počítadlo intervalov časovača.
volatile bool timeColumn = false;          // Príznak pre pridanie časového stĺpca do výstupných dát.

// Timer A0 interrupt service routine(ISR) - obsluha prerušenia časovača A0
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0_ISR(void) {
  intervalCounter++;  // Inkrementácia počtu intervalov časovača.

  if (intervalCounter % 1000 == 0) {
    // Akcie, ktoré sa majú vykonať každých 1000 intervalov (alebo každú sekundu).
    ADC12CTL0 |= ADC12SC;  // Spustenie konverzie ADC.
    LED2_TOGGLE();         // Prepnutie LED2 ako vizuálny indikátor.
  }

  if (intervalCounter % 5000 == 0) {
    // Akcie, ktoré sa majú vykonať každých 5000 intervalov (alebo každých 5 sekúnd).
    fiveSecCounter++;
    timeColumn = true;     // Nastavenie premennej na pridanie časovej značky.
    LED1_TOGGLE();         // Prepnutie LED1 ako vizuálny indikátor.
  }

  TA0CCTL0 &= ~CCIFG;      // Vyčistenie príznaku prerušenia pre pripravenosť na ďalšie prerušenie.
}

char largeBuffer[BUFFER_SIZE]; // Buffer to hold larger data sequences for processing.
int bufferIndex = 0;           // Index veľkého buffera

// ADC ISR
#pragma vector=ADC12_VECTOR
__interrupt void ADC12_ISR(void) {
  if (ADC12IV == 6) {  // Kontrola, či vektor prerušenia pochádza z ADC12MEM0.
    adcResult = ADC12MEM0;  // Získanie výsledku konverzie z pamäte ADC12.

    char dataBuffer[64];    // Dočasný buffer na formátovanie údajov.
    char numBuffer[32];     // Buffer pre numerické konverzie.
    unsigned int bytesWritten;  // Počet zapísaných bajtov.

    uint_to_str(adcResult, dataBuffer);  // Konverzia výsledku ADC na reťazec.
    strcat(dataBuffer, " ");             // Pripojenie medzery pre oddelenie hodnôt.

    if (timeColumn) {
      uint_to_str(fiveSecCounter, numBuffer);  // Konverzia časového počítadla na reťazec.
      strcat(dataBuffer, numBuffer);           // Pripojenie časového počítadla.
      strcat(dataBuffer, "\n");                // Pripojenie nového riadku na koniec záznamu.
      timeColumn = false;                      // Reset príznaku pre nasledujúce časové razítko.

      if (bufferIndex + strlen(dataBuffer) < BUFFER_SIZE) {
        strcpy(largeBuffer + bufferIndex, dataBuffer);  // Kopírovanie formátovaných údajov do veľkého buffera.
        bufferIndex += strlen(dataBuffer);              // Aktualizácia indexu buffera.
      }

      writeBufferToFile();  // Zapísanie bufferovaných údajov do súboru.
      clearBuffer();        // Vyčistenie buffera pre ďalšiu sadu údajov.
    } else {
      strcat(dataBuffer, "\n");  // Pripojenie iba nového riadku, ak nie je potrebné časové razítko.
      if (bufferIndex + strlen(dataBuffer) < BUFFER_SIZE) {
        strcpy(largeBuffer + bufferIndex, dataBuffer);  // Kopírovanie údajov do buffera.
        bufferIndex += strlen(dataBuffer);              // Aktualizácia indexu podľa potreby.
      }
    }


    LED1_TOGGLE();  // Prepnutie LED1 na indikáciu čítania ADC.
    uartSendString(dataBuffer);  // Odoslanie údajov cez UART pre externé monitorovanie.
  }
}

void writeBufferToFile() {
    unsigned int bytesWritten;  // Premenná pre počet zapísaných bajtov.
    FRESULT openResult = f_open(&file, "/data.txt", FA_OPEN_ALWAYS | FA_WRITE);  // Otvorenie súboru na zápis.
    if (openResult == FR_OK) {
        f_lseek(&file, f_size(&file));  // Posun na koniec súboru.
        f_write(&file, largeBuffer, bufferIndex, &bytesWritten);  // Zapísanie údajov.
        f_close(&file);  // Zatvorenie súboru na uloženie údajov.
    }
}

// Vyčistenie buffera po zapísaní do súboru
void clearBuffer() {
    memset(largeBuffer, 0, BUFFER_SIZE);  // Vyčistenie buffera
    bufferIndex = 0;                      // Reset indexu buffera
}

void fat_init(void){
  errCode = -1;  // Inicializácia kódu chyby na stav chyby.

  while (errCode != FR_OK) {  // Slučka pokračuje, kým funkcie súborového systému nevrátia úspech.
    errCode = f_mount(0, &fatfs);  // Pokus o pripojenie súborového systému.
    errCode = f_opendir(&dir, "/");  // Pokus o otvorenie koreňového adresára.

    errCode = f_open(&file, "/data.txt", FA_CREATE_ALWAYS | FA_WRITE);  // Pokus o otvorenie súboru na zápis.
    if (errCode != FR_OK)
      result = 0;  // Nastavenie výsledku na 0 v prípade zlyhania pre účely ladenia.
  }
}

void initCLK(void){
  volatile unsigned int i;

  // Zvýšenie nastavenia Vcore na úroveň 3 na podporu fsystem=25MHz
  // POZNÁMKA: Zmena jadrového napätia sa vykonáva postupne po úrovniach..
  SetVcoreUp (0x01);
  SetVcoreUp (0x02);
  SetVcoreUp (0x03);

  UCSCTL3 = SELREF_2;                       // Nastavenie referencie DCO FLL = REFO
  UCSCTL4 |= SELA_2;                        // Nastavenie ACLK = REFO

  __bis_SR_register(SCG0);                  // Deaktivácia riadiacej slučky FLL
  UCSCTL0 = 0x0000;                         // Nastavenie najnižších možných hodnôt DCOx, MODx
  UCSCTL1 = DCORSEL_7;                      // Výber rozsahu DCO pre 50MHz operáciu
  UCSCTL2 = FLLD_1 + 762;                   // Nastavenie multiplikátora DCO pre 25MHz
                                            // (N + 1) * FLLRef = Fdco
                                            // (762 + 1) * 32768 = 25MHz
                                            // Nastavenie FLL Div = fDCOCLK/2
  __bic_SR_register(SCG0);                  // Povolenie riadiacej slučky FLL

  // Najhorší prípad času nastavenia pre DCO, keď boli zmenené bity rozsahu DCO,
  // je n x 32 x 32 x f_MCLK / f_FLL_reference. Pozrite kapitolu UCS v UG pre 5xx pre optimalizáciu.
  // 32 x 32 x 25 MHz / 32,768 Hz ≈ 780k cyklov MCLK pre usadenie DCO
  __delay_cycles(782000);

  // Slučka kým sa XT1, XT2 & DCO nestabilizujú - v tomto prípade sa musí stabilizovať iba DCO
  do {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);  // Vyčistenie príznakov chýb XT2, XT1, DCO
    SFRIFG1 &= ~OFIFG;                          // Vyčistenie príznakov chýb
  } while (SFRIFG1 & OFIFG);                     // Testovanie príznaku chyby oscilátora
}

void SetVcoreUp (unsigned int level)
{
  // Otvorenie PMM registrov pre zápis
  PMMCTL0_H = PMMPW_H;
  // Nastavenie SVS/SVM na vyššiu úroveň
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
  // Nastavenie SVM na nižšiu úroveň
  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
  // Čakanie, kým sa SVM ustáli
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);          // Vyčistenie už nastavených príznakov.
  PMMCTL0_L = PMMCOREV0 * level;              // Nastavenie VCore na novú úroveň.
  if (PMMIFG & SVMLIFG)                       // Kontrola, či došlo k poklesu napätia na nižšej strane.
    while ((PMMIFG & SVMLVLRIFG) == 0);
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level; // Nastavenie SVS/SVM na nižšej strane na novú úroveň.
  PMMCTL0_H = 0x00;                           // Zamknutie PMM registrov opäť.
}
